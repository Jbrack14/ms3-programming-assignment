# README #

Jordan Brack  ---  jabrack@mix.wvu.edu

### MySQL Database Connector / CSV Ingester ###

* Ingests a .csv file and parses data into MySQL database. Ignores any records in the .csv file with the incorrect number of elements.

### Setup ###

* Running MySQL Server version: 5.5.54-0ubuntu0.14.04.1 (Ubuntu)
* Create necessary database and table with:

   >$ mysql -u root -ppassword < createMyDB.sql