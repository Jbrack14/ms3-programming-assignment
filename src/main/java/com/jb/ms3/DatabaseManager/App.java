package com.jb.ms3.DatabaseManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

public class App 
{
	private static String timestamp; 
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
    
    public static void main( String[] args ) throws SQLException
    {
        System.out.println( "Initializing Database Connection..." );
        DatabaseConnector dbc = new DatabaseConnector();
        dbc.initConnection();
        Timestamp startTime = new Timestamp(System.currentTimeMillis());
        timestamp = (sdf.format(startTime));
        System.out.println( "Parsing CSV..." );
        readCSV(dbc);
    }    
    
    public static void readCSV(DatabaseConnector dbc)
    {
        try {
        	List<String[]> badData = new ArrayList<String[]>();
        	String csvFile = "ms3Interview.csv";
            int total = 0, failed = 0;
            CSVReader reader = null;
            reader = new CSVReader(new FileReader(csvFile));
            String[] line;
            while ((line = reader.readNext()) != null) {
            	total++;
            	if(line.length != 10)
            	{
            		failed++;
            		System.out.println("Line " + total + " failed! Record dropped.");
            		badData.add(line);
            	}
            	else
            	{
            		dbc.insertRecord(line);
            	}
            }
            reader.close();
            writeToLog(total, failed);
            writeCSV(badData);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
			e.printStackTrace();
		}
    }
    
    public static void writeCSV(List<String[]> data)
    {
    	CSVWriter writer;
    	String badCSVPath = "bad-data-" + timestamp + ".csv";
		try {
			writer = new CSVWriter(new FileWriter(badCSVPath), ',');
		        writer.writeAll(data);
		   	 	writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
      
    }
    
    public static void writeToLog(int total, int failed)
    {
        Logger logger = Logger.getLogger("MyLog");
        FileHandler fh;
         
        try {
            
            fh = new FileHandler("MyLogFile.log");
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
             
            logger.info("\nTotal records recieved: " + total + "\nSuccessful: " + (total - failed) + "\nFailed: " + failed);
             
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
   
}
