package com.jb.ms3.DatabaseManager;

import java.sql.*;


public class DatabaseConnector {
	private Connection connect = null;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;
	
	private static final String DB_URL = "jdbc:mysql://localhost/";
	private static final String DB_NAME = "mysqldb";
	private static final String DB_TABLE = "mytable";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "password";

	public void initConnection() throws SQLException
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		connect = DriverManager.getConnection(DB_URL + DB_NAME + "?user=" + DB_USER + "&password=" + DB_PASSWORD);
	}
	
	public void readDatabase() throws SQLException
	{
		statement = connect.createStatement();
		resultSet = statement.executeQuery("select * from " + DB_NAME + "." + DB_TABLE);
		printResultSet(resultSet);
	}
	
	public void insertRecord(String[] line) throws SQLException
	{
	    String query = " insert into " + DB_TABLE + " (a, b, c, d, e, f, g, h, i, j)"
	        + " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	    preparedStatement = connect.prepareStatement(query);
		
	    for(int i=0; i < 10; i++)
	    {
	    	preparedStatement.setString(i+1, line[i]);
	    }
		preparedStatement.execute();
	}
	
	private void printResultSet(ResultSet resultSet) throws SQLException {
		int i = 0;
        while (resultSet.next()) {
            	System.out.println("--- Row " + i + " ----------------------------------");
            	
                String A = resultSet.getString("A");
                String B = resultSet.getString("B");
                String C = resultSet.getString("C");
                String D = resultSet.getString("D");
                String E = resultSet.getString("E");
                String F = resultSet.getString("F");
                String G = resultSet.getString("G");
                String H = resultSet.getString("H");
                String I = resultSet.getString("I");
                String J = resultSet.getString("J");
                
                System.out.println("A: " + A);
                System.out.println("B: " + B);
                System.out.println("C: " + C);
                System.out.println("D: " + D);
                System.out.println("E: " + E);
                System.out.println("F: " + F);
                System.out.println("G: " + G);
                System.out.println("H: " + H);
                System.out.println("I: " + I);
                System.out.println("J: " + J);
                i++;
        }
	}
    
    private void closeConnection() {
    	try {
	        if (resultSet != null) {
	                resultSet.close();
	        }
	
	        if (statement != null) {
	                statement.close();
	        }
	
	        if (connect != null) {
	                connect.close();
	        }
        } catch (Exception e) {
        	
        }
    }
}
