create database mysqldb;

use mysqldb;

create table mytable (
	A VARCHAR(30),
	B VARCHAR(30),
	C VARCHAR(30),
	D VARCHAR(30),
	E VARCHAR(255),
	F VARCHAR(30),
	G VARCHAR(30),
	H VARCHAR(30),
	I VARCHAR(30),
	J VARCHAR(30));
