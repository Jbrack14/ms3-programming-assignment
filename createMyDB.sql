create database mysqldb;

use mysqldb;

create table mytable (
	A VARCHAR(255),
	B VARCHAR(255),
	C VARCHAR(255),
	D VARCHAR(255),
	E VARCHAR(5000),
	F VARCHAR(255),
	G VARCHAR(255),
	H VARCHAR(255),
	I VARCHAR(255),
	J VARCHAR(255));
